function formValidation() {
    var flag = 1;

    var alpha = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;

    var mail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var companyName = document.getElementById('companyName').value;
    var mobile = document.getElementById('mobileName').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;

    if (companyName == "") {
        document.getElementById('companyNames').innerHTML = "*Please Fill the company Name*";
        flag = 0;
    }
    // if((companyName.length <=15) || (companyName>25)){
    //   document.getElementById('companyNames').innerHTML = "*company name must be between 15 and 25 *";
    //   return false;
    // }
    else if (companyName.length <= 15) {
        document.getElementById('companyNames').innerHTML = "*company name at least 15*";
        flag = 0;
    } else if (!isNaN(companyName)) {
        document.getElementById('companyNames').innerHTML = "*Number are not allowed*";
        flag = 0;

    } else {
        document.getElementById('companyNames').innerHTML = "";
        flag = 1;
    }

    if (mobile == "") {
        document.getElementById('mobileNames').innerHTML = "*Please Fill mobile no*";
        flag = 0;
    } else if (isNaN(mobile)) {
        document.getElementById('mobileNames').innerHTML = "*Character is not allow only number*";
        flag = 0;
    } else if (mobile.length != 10) {
        document.getElementById('mobileNames').innerHTML = "*mobile number must be atleat 10 digits *";
        flag = 0;
    } else {
        document.getElementById('mobileNames').innerHTML = "";
        flag = 1;

    }

    if (email == "") {
        document.getElementById('emails').innerHTML = "*Please Fill the email*";
        flag = 0;
    } else if (!email.match((mail))) {
        document.getElementById('emails').innerHTML = "*@ invalid position*";
        flag = 0;
    } else {
        document.getElementById('emails').innerHTML = "";
        flag = 1;

    }

    if (password == "") {
        document.getElementById('passwords').innerHTML = "*Please Fill the password*";
        flag = 0;
    } else if (!password.match((alpha))) {
        document.getElementById('passwords').innerHTML = "*Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters *";
        flag = 0;
    } else {
        document.getElementById('passwords').innerHTML = "";
        flag = 1;
    }

    if (confirmPassword == "") {
        document.getElementById('confirmPasswords').innerHTML = "*Please Fill confirm password*";
        flag = 0;
    } else if (password != confirmPassword) {
        document.getElementById('confirmPasswords').innerHTML = "*Password are not matching*";
        flag = 0;
    } else {
        document.getElementById('confirmPasswords').innerHTML = "*Password are not matching*";
        flag = 0;
    }
    if (flag) {
        return true;
    } else {
        return false;
    }

}